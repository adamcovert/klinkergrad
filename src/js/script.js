$(document).ready(function(){

  // Slider
  $('.owl-carousel').owlCarousel({
    items: 1,
    dots: false,
    nav: true,
    navText: ['<img src="img/arrow-left.png" alt="" />','<img src="img/arrow-right.png" alt="" />'],
    loop: true,
    smartSpeed: 700,
  });
});

// Prices tabs
var tabs = document.querySelectorAll('.prices__nav .prices__nav-item a');
var contents = document.querySelectorAll('.prices__content .prices__content-item');

function changeTab(event) {
  for (var i=0; i<tabs.length; i++){
    tabs[i].parentNode.classList.remove('is-active');
  }
  event.target.parentNode.classList.add('is-active');

  for (var i=0; i<contents.length; i++) {
    contents[i].classList.remove('is-active');
  }
  var link = event.target.getAttribute('href');
  document.querySelector(link).classList.add('is-active');
}

for (var i=0; i<tabs.length; i++){
  tabs[i].addEventListener('click', changeTab);
}